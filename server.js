const express        = require('express');
const admin          = require('firebase-admin');
const bodyParser     = require('body-parser');

var crypto = require('crypto')

const request = require('request');
const app            = express();
const port           = 8000;
var serviceAccount = require("./closed-air-firebase-adminsdk-etxbk-e6b8a6eb5f.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://closed-air.firebaseio.com"
});

const db = admin.database();
var ref = db.ref();


app.use(bodyParser.urlencoded({ extended: true }));


app.listen(port, () => {
  console.log('We are live on ' + port);
});

app.get('/', function (req, res) {
  ref.once("value", function(snapshot) {
    res.send(snapshot.val());
    console.log(snapshot.val());
  });
});

app.get('/firstInit', function(req, res){
  var defaultData = {
    "connected" : true,
    "curentVolume" : 90,
    "preDelay" : 50,
    "afterDelay" : 50,
    // "ip" : "255.255.255.255",
    "maxVolume" : 100,
    "mode" : "close",
    "states": {
      "askConnection" : false,
      "changeMode" : false,
      "getCurrentVolume" : false,
      "maxVolumeChanged" : false,
      "preDelayChanged" : false,
      "afterDelay": false
    }
  }
  // var ip = req.body.ip;
  // var id = reg.body.arduinoId;
  var ip = '224.224.224.223';
  var id = randomValueHex(12);
  defaultData['ip'] = ip;

  ref.child(id).update(defaultData);
  res.send('first Init');
  console.log('first Init success');
  // При первом запуске приходят данные от Ардуино, а мы в ответ присылаем пороговое значение и создаём всё в базе данных (ветки, IP) и так далее
})

ref.on("child_changed", function(snapshot) {
  console.log('Child Changed')
  console.log('=============')
  var snapshotData = snapshot.val();
  var deviceId = snapshot.key;
  var deviceRef = db.ref(deviceId);

  if(snapshotData.states.askConnection){
    request.post({url: 'https://webhook.site/c3a11670-ffd3-4192-a53c-951ea52d2ac3', form: {key:'value', testData: 'test text'}}, function optionalCallback(err, res, body) {
      if (err) { return console.log(err); }
      console.log(body);
      console.log('response OK');
      deviceRef.child('/states').update({askConnection: false});
      deviceRef.update({connected: true});
    });
  }else if (snapshotData.states.changeMode) {
    request('https://webhook.site/c3a11670-ffd3-4192-a53c-951ea52d2ac3', { json: true }, (err, res, body) => {
      if (err) { return console.log(err); }
      console.log('New mode = ' + snapshotData.mode);
      deviceRef.child('/states').update({changeMode: false});
    });
  }else if (snapshotData.states.sendNotificationAboutRain){
    // Скорее всего будет просто изменение state на firebase и в итоге покажет уведомление
  }else if (snapshotData.states.getCurrentVolume){
    // Если ришёл запрос на постоянный замер дынных, вызываем функцию, из которой выходим только тогда, когда snapshotData.getCurrentVolume == false
  }else if (snapshotData.states.maxVolumeChanged) {
    request('https://webhook.site/c3a11670-ffd3-4192-a53c-951ea52d2ac3', { json: true }, (err, res, body) => {
      if (err) { return console.log(err); }
      console.log('New maxVolume = ' + snapshotData.maxVolume);
      deviceRef.child('/states').update({maxVolumeChanged: false});
    });
  }else if (snapshotData.states.preDelayChanged) {
    request('https://webhook.site/c3a11670-ffd3-4192-a53c-951ea52d2ac3', { json: true }, (err, res, body) => {
      if (err) { return console.log(err); }
      console.log('New preDelay = ' + snapshotData.preDelay);
      deviceRef.child('/states').update({preDelayChanged: false});
    });
  }else if (snapshotData.states.afterDelayChanged) {
    request('https://webhook.site/c3a11670-ffd3-4192-a53c-951ea52d2ac3', { json: true }, (err, res, body) => {
      if (err) { return console.log(err); }
      console.log('New afterDelay = ' + snapshotData.afterDelay);
      deviceRef.child('/states').update({afterDelayChanged: false});
    });
  }


}, function (errorObject) {
  console.log("The read failed: " + errorObject.code);
});



function randomValueHex(len) {
  return crypto
    .randomBytes(Math.ceil(len / 2))
    .toString('hex') // convert to hexadecimal format
    .slice(0, len) // return required number of characters
}
